import GameOfLife from "../src/GameOfLife";
import {describe, expect, it} from "@jest/globals";

describe("GameOfLife", () => {
    it("should transform vertical blinker into horizontal blinker", () => {
        const blinker: Array<string> = [
            'o', 'x', 'o',
            'o', 'x', 'o',
            'o', 'x', 'o',
        ];
        const gameOfLife: GameOfLife = new GameOfLife(blinker, 3, 3);

        const oneYearLater = gameOfLife.iterate(1);

        expect(oneYearLater).toEqual([
                [
                    'o', 'o', 'o',
                    'x', 'x', 'x',
                    'o', 'o', 'o',
                ]
            ]
        );
    });

    it("should transform vertical toad into horizontal toad", () => {
        const toad: Array<string> = [
            'o', 'o', 'o', 'o',
            'o', 'x', 'x', 'x',
            'x', 'x', 'x', 'o',
            'o', 'o', 'o', 'o',
        ];
        const gameOfLife: GameOfLife = new GameOfLife(toad, 4, 4);


        const oneYearLater = gameOfLife.iterate(1);

        expect(oneYearLater).toEqual(
            [
                [
                    'o', 'o', 'x', 'o',
                    'x', 'o', 'o', 'x',
                    'x', 'o', 'o', 'x',
                    'o', 'x', 'o', 'o',
                ]
            ]
        );
    });
});
