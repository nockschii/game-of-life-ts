# sample-project-ts

TypeScript sample project with Jest.

# Install

run `npm i`

# run tests

Either in the IDE or `npm test` (with coverage)

# useful docs

https://jestjs.io/docs/expect

https://sinonjs.org/releases/latest/
