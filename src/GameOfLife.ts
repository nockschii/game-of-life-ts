export default class GameOfLife {
    private previousGrid: Array<string>;
    private readonly width: number;
    private height: number;
    private nextGrid: Array<string> = [];
    private nextGenerations: Array<Array<string>> = [];

    constructor(grid: Array<string>, width: number, height: number) {
        this.previousGrid = grid;
        this.width = width;
        this.height = height;
    }

    iterate(years: number): Array<Array<string>> {
        let count = 0;
        while (count < years) {
            this.previousGrid.forEach((cell, position) => {
                this.forecastNextGeneration(cell,position);
            });

            this.nextGenerations.push(this.nextGrid);
            this.previousGrid = this.nextGrid;
            this.nextGrid = [];
            count++;
        }

        return this.nextGenerations;
    }

    private forecastNextGeneration(cell: string, position: number) {
        const neighboursCount = this.countNeighbours(position);
        this.nextGrid[position] = this.applyRules(neighboursCount, position);
    }

    private countNeighbours(position: number): number {
        let neighboursCount = 0;

        if (!this.isRightEdge(position)) {
            neighboursCount += this.incrementOnLivingNeighbour(position + 1);
        }

        if (!this.isRightEdge(position) && !(this.isBottomEdge(position))) {
            neighboursCount += this.incrementOnLivingNeighbour(position + this.width + 1);
        }

        if (!this.isBottomEdge(position)) {
            neighboursCount += this.incrementOnLivingNeighbour(position + this.width);
        }

        if (!(this.isLeftEdge(position)) && !(this.isBottomEdge(position))) {
            neighboursCount += this.incrementOnLivingNeighbour(position + this.width - 1);
        }

        if (!this.isLeftEdge(position)) {
            neighboursCount += this.incrementOnLivingNeighbour(position - 1);
        }

        if (!(this.isLeftEdge(position)) && !(this.isTopEdge(position))) {
            neighboursCount += this.incrementOnLivingNeighbour(position - this.width - 1);
        }

        if (!this.isTopEdge(position)) {
            neighboursCount += this.incrementOnLivingNeighbour(position - this.width);
        }

        if (!(this.isRightEdge(position) && !(this.isTopEdge(position)))) {
            neighboursCount += this.incrementOnLivingNeighbour(position - this.width + 1);
        }

        return neighboursCount;
    }

    private incrementOnLivingNeighbour(position: number): number {
        if (!this.isAlive(position)) {
            return 0;
        }

        return 1;
    }

    private isAlive(position: number) {
        return this.previousGrid[position] === 'x';
    }

    private isLeftEdge(position: number): boolean {
        return (position + 1) % this.width === 1;
    }

    private isRightEdge(position: number): boolean {
        return (position + 1) % this.width === 0;
    }

    private isTopEdge(position: number): boolean {
        return (position + 1) < this.width;
    }

    private isBottomEdge(position: number): boolean {
        return (position + 1) >  this.previousGrid.length - this.width;
    }

    private applyRules(neighboursCount: number, position: number): string {
        if (this.previousGrid[position] === "o") {
            if (neighboursCount === 3) {
                return "x";
            }

            return "o";
        } else {
            if (neighboursCount === 2 || neighboursCount === 3) {
                return "x";
            }

            return "o";
        }
    }
}
